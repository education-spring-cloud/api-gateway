# Zuul API Gateway

## Docker 
To build image from this directory:
```
docker build --tag gopas/api-gateway .
```
And run it:
```
docker run -d --rm --name gateway01 -p80:80 --hostname gateway01 gopas/api-gateway --link eureka01:eurekaserver
```
Show logs:
```
docker logs -f gateway01
```
Stop it:
```
docker stop gateway01 
```
Remove container:
```
docker rm gateway01
```